package com.evaluation.suivi.Resource;

import com.evaluation.suivi.Services.SalleServices;
import com.evaluation.suivi.entites.Salle;
import java.util.List;
import static org.glassfish.jersey.internal.inject.Bindings.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author koiranga
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class SalleResource {
    @Autowired
    private SalleServices service;
    
   // @PostMapping("/ajoutersal")
     @RequestMapping(value = "/AjouterSalle", method = RequestMethod.GET)
    public Salle AddSal(@RequestBody Salle salle) {
        return service.addSalle(salle);
    }
    

    @GetMapping("/salles")
    public List<Salle> findAllSalles() {
        return service.listeSalle();
    }
    
}