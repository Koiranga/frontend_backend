package com.evaluation.suivi.Resource;
import com.evaluation.suivi.Services.UniteServices;
import com.evaluation.suivi.entites.Unite;
import java.util.List;
import javax.ws.rs.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author koiranga
 */

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class UniteResource {
    @Autowired
    private UniteServices service;
    
    @RequestMapping(value = "/AjouterUnite", method = RequestMethod.POST)
    // @PostMapping("/Ajoutunite")
    public Unite AddUnite(@RequestBody Unite unite) {
        return service.addUnite(unite);
    }

    @RequestMapping(value = "/ListeUnite}", method = RequestMethod.GET)
    public List<Unite> getUniteByNiveauOption(@PathVariable int niveau, @PathVariable String option) {
        return service.listeUnite(niveau,option);
    }
    @RequestMapping(value = "/ChercherUnite/{intitule}", method = RequestMethod.GET)
    public Unite SearchUnite(@PathVariable String intitule) {
        return service.getUniteByIntitule(intitule);
    }
     @RequestMapping(value = "/ModifierUnite", method = RequestMethod.GET)
//    @PutMapping("/updateunite")
    public Unite UpdateEva(@RequestBody Unite unite) {
        return service.modifierUnite(unite);
    }
    
}
