package com.evaluation.suivi.Resource;
import com.evaluation.suivi.Services.EvaluationServices;
import com.evaluation.suivi.entites.Evaluation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author koiranga
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class EvaluationRessource {

    @Autowired
    private EvaluationServices service;

    @GetMapping("/ChercherEvaluation")
    public List<Evaluation> findAllEvaluations() {
        return service.listeEvaluation();
    }
    
    
//    @PutMapping("/updateeva")
     @RequestMapping(value = "/ModifierEvaluation", method = RequestMethod.GET)
    public Evaluation UpdateEva(@RequestBody Evaluation eva) {
        return service.modifierEvaluation(eva);
    }

}