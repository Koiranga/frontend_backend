package com.evaluation.suivi.Resource;
import com.evaluation.suivi.Services.UserServices;
import com.evaluation.suivi.entites.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author koiranga
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class UserResource { 
@Autowired
private UserServices service;

// @PostMapping("/ajouterUser")
 @RequestMapping(value = "/AjouterUser", method = RequestMethod.GET)
    public User AddUse(@RequestBody User user) {
        return service.addUser(user);
    }

    
//    @PutMapping("/updateuse")
     @RequestMapping(value = "/ModifierUser", method = RequestMethod.GET)
    public User UpdateUse(@RequestBody User user) {
        return service.modifierUser(user);
    }
}
