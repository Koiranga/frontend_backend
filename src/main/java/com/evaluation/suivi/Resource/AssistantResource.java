package com.evaluation.suivi.Resource;


import com.evaluation.suivi.Services.AssistantServices;
import com.evaluation.suivi.entites.Assistant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author koiranga
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class AssistantResource {
     @Autowired
    private AssistantServices service;

   //  @PostMapping("/AjouterAssistant")
      @RequestMapping(value = "/AjouterAssistant", method = RequestMethod.GET)
    public Assistant AddAssis(@RequestBody Assistant assistant) {
        return service.addAssistant(assistant);
    } 

    @GetMapping("/ChercherAssistant")
    public Assistant findAllAssistant(String nom) {
        return service.searchAssistant(nom);
    }
    
    
//    @PutMapping("/updateassist")
     @RequestMapping(value = "/ModifierAssistant", method = RequestMethod.GET)
    public Assistant UpdateAssis(@RequestBody Assistant assistant) {
        return service.modifierAssistant(assistant);
    } 
    
}
