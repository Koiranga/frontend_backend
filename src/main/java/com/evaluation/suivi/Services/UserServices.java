/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluation.suivi.Services;

import com.evaluation.suivi.dao.UserDao;
import com.evaluation.suivi.entites.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author koiranga
 */
@Service
public class UserServices {
    @Autowired
    private UserDao menga;
 public User addUser(User user){
        return menga.save(user);
        }
      public User modifierUser(User user){
       User existingUser = menga.findById(user.getIdUser()).orElse(null);
          existingUser.setNom(user.getNom());
          existingUser.setPassword(user.getPassword());
          return menga.save(existingUser);
      }
}
