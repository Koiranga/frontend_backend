/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluation.suivi.Services;

import com.evaluation.suivi.dao.SalleDao;
import com.evaluation.suivi.entites.Salle;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author koiranga
 */
@Service
public class SalleServices {

   @Autowired
   private SalleDao sall;
    
    public Salle addSalle(Salle salle){
    return sall.save(salle);
    }
   public List<Salle> listeSalle(){
          return sall.findAll();
      } 
   
}
