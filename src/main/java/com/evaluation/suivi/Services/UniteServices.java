
package com.evaluation.suivi.Services;

import com.evaluation.suivi.dao.UniteDao;
import com.evaluation.suivi.entites.Unite;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author koiranga
 */
@Service
     public class UniteServices {
         
     @Autowired
    private UniteDao unit;
        public Unite addUnite(Unite unite){
        return unit.save(unite);
        }
      public List<Unite> listeUnite(int niveau, String option){
          return unit.findByNiveauAndOption(niveau,option);
      }  
      public Unite getUniteByIntitule(String intitule){
      return unit.findUniteByIntitule(intitule);
      }
      public Unite modifierUnite(Unite unite){
       Unite existingUnite = unit.findById(unite.getId()).orElse(null);
          existingUnite.setCodeUE(unite.getCodeUE());
          existingUnite.setIntitule(unite.getIntitule());
          return unit.save(existingUnite);
      }
}
