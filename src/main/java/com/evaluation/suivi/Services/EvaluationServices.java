/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluation.suivi.Services;

import com.evaluation.suivi.dao.EvaluationDao;
import com.evaluation.suivi.entites.Evaluation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author koiranga
 */
@Service
public class EvaluationServices {
    @Autowired
    private EvaluationDao eva;
       
//        public Evaluation addEvaluation(Evaluation evaluation){
//        return eva.save(evaluation);
//        }
      public List<Evaluation> listeEvaluation(){
          return eva.findAll();
      }  
      public Evaluation modifierEvaluation(Evaluation evaluation){
       Evaluation existingEvaluation = eva.findById(evaluation.getIdEva()).orElse(null);
          existingEvaluation.setNbreCopie(evaluation.getNbreCopie());
          existingEvaluation.setNbreEtudiant(evaluation.getNbreEtudiant());
          existingEvaluation.setRapport(evaluation.getRapport());
          return eva.save(existingEvaluation);
      }
       
}
