/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluation.suivi.Services;

import com.evaluation.suivi.dao.AssistantDao;
import com.evaluation.suivi.entites.Assistant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author koiranga
 */
@Service
public class AssistantServices {
    @Autowired
    private AssistantDao assis ;
  
  public Assistant searchAssistant(String nom){
  return assis.findByNom(nom);
  }
  public Assistant addAssistant(Assistant assistant){
  return assis.save(assistant);
  
  }
public Assistant modifierAssistant(Assistant assistant){
Assistant existingAssistant = assis.findById(assistant.getId()).orElse(null);
          existingAssistant.setNom(assistant.getNom());
          existingAssistant.setPrenom(assistant.getPrenom());
          return assis.save(existingAssistant);
}  
}
