
package com.evaluation.suivi.entites;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;

/**
 *
 * @author koiranga
 */
@Entity
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Assistant implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name="Nom",nullable = false)
    private String nom;
    
     @Column(name="Prenom",nullable = false)
    private String prenom;
     
     @JsonIgnore
    @XmlTransient
     @ManyToMany
     @JoinTable(name="assistant_evaluation",joinColumns=@JoinColumn(name="id"),
            inverseJoinColumns=@JoinColumn(name="idEva"))
    private List<Evaluation>evaluation;

}
