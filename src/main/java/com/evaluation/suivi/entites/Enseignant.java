/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluation.suivi.entites;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;
/**
 *
 * @author koiranga
 */
@Entity
@Data
public class Enseignant extends Assistant{
//    
    
    @Column(name="_grade",nullable = false)
    private String grade;
    
    @Column(name="_fonctionEns",nullable = false)
    private String fonctionEns;
}

