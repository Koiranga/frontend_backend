/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluation.suivi.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;

/**
 *
 * @author koiranga
 */
@Entity
@Data
public class Salle implements Serializable{
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private String numSalle;
    
    @Column(name="nbrePlace", nullable=false)
    private String nbrePlace;
    
     @JsonIgnore
    @XmlTransient
 
    @OneToMany(mappedBy="salle")
    private List<Evaluation> evaluations;

    
    
   
}
