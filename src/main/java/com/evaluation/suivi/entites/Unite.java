/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluation.suivi.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;
/*
 *
 * @author koiranga
 */
@Entity
@Data
public class Unite implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Column(name="_codeUE", nullable=false)
    private String codeUE;
    
    @Column(name="_intitule", nullable=false)
    private String intitule;
    
    @Column(name="_niveau", nullable=false)
    private int niveau;
    
    @Column(name="_semestre", nullable=false)
    private int semestre;
    
    @Column(name="_option", nullable=false)
    private String option;
    
    @Column(name="_parcours", nullable=false)
    private String parcours;
    
    @JsonIgnore
    @XmlTransient
     @OneToMany(mappedBy="unite")
    private List<Evaluation> evaluation;

   
    
}

