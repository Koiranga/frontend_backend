package com.evaluation.suivi.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;

/**
 *
 * @author koiranga
 */
@Entity
@Data
public class AnneeAcademique implements Serializable{
    
    @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idAca;
    
    @Column(name="anneeAca",nullable = false)
    private String anneeAca;
   
   @JsonIgnore
    @XmlTransient
   
  @OneToMany(mappedBy="anneeAcademique")
   private List<Evaluation> evaluation;

}
