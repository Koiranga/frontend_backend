/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluation.suivi.dao;

import com.evaluation.suivi.entites.Assistant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

/**
 *
 * @author koiranga
 */
@Repository
public interface AssistantDao extends JpaRepository <Assistant, Long>{

    public Assistant findByNom(String nom);
    
     
    
}

