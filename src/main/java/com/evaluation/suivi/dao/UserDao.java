/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evaluation.suivi.dao;

import com.evaluation.suivi.entites.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author koiranga
 */
@Repository
public interface UserDao extends JpaRepository<User, Long> {
    
    
}
