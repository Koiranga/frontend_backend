import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Login } from './login';
@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {
   
  constructor(private http: HttpClient) { }
  public login(user :Login):Observable<any> {
    return this.http.post<any>('http://localhost:9193/api/login',user);

}
}
