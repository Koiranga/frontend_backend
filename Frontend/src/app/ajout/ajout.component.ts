import { Component, OnInit } from '@angular/core';
import { Unite } from '../ServiceUnite/unite';
import { UniteService } from '../ServiceUnite/unite.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-ajout',
  templateUrl: './ajout.component.html',
  styleUrls: ['./ajout.component.css']
})
export class AjoutComponent implements OnInit {
  unites: Unite[];
  unite: Unite;
 
  constructor(private uniteservice: UniteService, private router: Router) { }

  ngOnInit() {
  }
  Ajouter() {
    this.uniteservice.Addunite(this.unite).subscribe(
      data => {
        console.log("Unite ajoute avec success");
        this.router.navigate(['/list'])
      },
      error => {
        console.log("exception occured");

      }
    )
  }
}
