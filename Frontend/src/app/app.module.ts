import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { AjoutComponent } from './ajout/ajout.component';
import { ListeComponent } from './liste/liste.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { UniteComponent } from './unite/unite.component';
import { FicheuniteComponent } from './ficheunite/ficheunite.component';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  { path : '', component : ConnexionComponent }, 
  { path : 'ajout', component : AjoutComponent },
  { path : 'list', component : ListeComponent },
  { path : 'ficheue', component : FicheuniteComponent },
  { path : 'acceuil', component : AcceuilComponent },
  { path : 'login', component : ConnexionComponent }
  
  
];
@NgModule({
  declarations: [
    AppComponent,
    AcceuilComponent,
    AjoutComponent,
    ListeComponent,
    FooterComponent,
    HeaderComponent,
    ConnexionComponent,
    UniteComponent,
    FicheuniteComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
