import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheuniteComponent } from './ficheunite.component';

describe('FicheuniteComponent', () => {
  let component: FicheuniteComponent;
  let fixture: ComponentFixture<FicheuniteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheuniteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheuniteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
