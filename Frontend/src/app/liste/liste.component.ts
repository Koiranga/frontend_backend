import { Component, OnInit } from '@angular/core';
import { Unite } from '../ServiceUnite/unite';
import { UniteService } from '../ServiceUnite/unite.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.css']
})
export class ListeComponent implements OnInit {
  unites: Unite[];
  unite: Unite;
  public niveau: number;
  public option: string;
  public intitule: string;
  constructor(private uniteservice: UniteService, private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }
  reloadData() {
    this.uniteservice.getUniteList(this.niveau, this.option).subscribe(data => {
      this.unites = data;
    })
    //this.regions=this.regionService.getRegionList();
  }
 
Chercher(intitule:string){
  this.uniteservice.getUnite(this.intitule).subscribe(data => {
    this.unites = data;

  })
}
ficheUnite(){
  this.router.navigate(['/ficheue'])
}
}