import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginserviceService } from '../ServiceConnexion/loginservice.service'
import { Login } from '../ServiceConnexion/login';
import { Router } from '@angular/router';
@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {
  user =  new Login();
  msg ='';
  constructor(private service:LoginserviceService, private router : Router) { }

  ngOnInit(): void {
  }
  loginAdmin (){
    this.service.login(this.user).subscribe(
      data => {console.log("reponse recu");
         this.router.navigate(['/acceuil'])
    },
      error => {console.log("exception occured");
      this.msg="nom d'utilisateur ou mot de passe incorrect";
  }
    )

}

}

  