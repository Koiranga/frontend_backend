import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Unite } from './unite';
@Injectable({
  providedIn: 'root'
})
export class UniteService {
constructor(private http: HttpClient) { }
 Addunite(unite :Unite):Observable<any> {
  return this.http.post<any>('http://localhost:9193/api/AjouterUnite',unite);

}
getUniteList(Niveau:number,Option:string):Observable<Unite[]> {
  return this.http.get<Unite[]>( 'http://localhost:9193/api/ListeUnite+"?Niveau="+niveau+"&Option="+option');
}
getUnite(intitule:String): Observable<any> {
  return this.http.get('http://localhost:9193/api/ChercherUnite/{Intitule}');
}


}
