import { Component, OnInit } from '@angular/core';
import { Unite } from '../ServiceUnite/unite';
import { UniteService } from '../ServiceUnite/unite.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.css']
})
export class AcceuilComponent implements OnInit {
  unite: Unite;
  public niveau: number;
  public option: string;
  
  constructor(private uniteservice: UniteService, private router: Router) { }

  

  ngOnInit() {
    this.reloadData();
  }
  reloadData() {
    this.uniteservice.getUniteList(this.niveau, this.option).subscribe(data => {
      this.router.navigate(['/list'])
    })
}
}