import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { AppComponent } from './app.component';
import { ConnexionComponent } from './connexion/connexion.component';


@NgModule({
  imports: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
